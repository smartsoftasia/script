#!/bin/bash


######################CONSTNT################################

LOG="backup.log" #log file

SQLPASS="WTFBeer69" #password
SQLHOST="localhost" #hostname
WF="/var/www" #Website folder
SF="shared" #path to the shared folder on the web appli folder
BF="/var/backup" #backup folder
SQLBF="$BF/sql" #sql backup folder
SHAREDBF="$BF/shared" #shared backup folder
DBS="" # Store list of databases
LIMIT="limit" #Use for get time now -1 month
#get the date
NOW="$(date +"%y%m%d")"

# DO NOT BACKUP these databases
IGGY="perforimance_schema information_schema mysql"

#DO NOT BACKUP these website folders, do not forget '.'
BLWEB=". ftp  " #Black liste /var/www folder

#BACKP these share folder
WLSHARE="log config system" #white list share folder

#####################FUNCTION###################################

#clearOldFiles
#$1 - folder to clear
function clearOldFiles () {
  if [ $# == 1 ];
  then
      #clear old files
      LIST=$(ls $1)
      for file in $LIST
      do
	#delete backup files if they are older than 30 days
	if [ $LIMIT -nt $1/$file ];
	then
	  echo "remove $file" >> $LOG
	  rm $1/$file
	fi
      done
      return 0
  else
    return 2
  fi
}

#blackList
#$1 - blackList
#$2 - arg to check if it's on the blacklist
#$3 - skip variable 
function blackList () {
  BL=$1
  if [ $# == 3 ];
  then
    if [ "${!BL}" != "" ];
    then
      for i in ${!BL}
      do
	#skip this database if it's on the blacklist
	[ "$2" == "$i" ] && eval $3=1 || :
      done
    fi
    return 0
  else
    return 2
  fi
}

#####################SCRIPT#################################

echo "==== Backup $NOW ====" >> $LOG

#create folder if not exist
[ ! -d $SQLBF ] && mkdir -p $SQLBF || :

#create folder if not exist
[ ! -d $SHAREDBF ] && mkdir -p $SHAREDBF || :

#create file at time now - 1 month
touch -d '-1 month' $LIMIT

# Get all database
DBS="$( mysql -h $SQLHOST -p$SQLPASS -Bse 'show databases')" 2>> $LOG
#DBS="$( sudo mysql -Bse 'show databases')"

echo "== database ==  " >>$LOG

for db in $DBS
do
  CF="$SQLBF/$db" #Curent folder
  #check if the database is on the skip list
  skipdb=-1
  blackList IGGY $db skipdb
  if [ $? != 0 ]; then echo "[ERROR] check blackList $db, wrong number of arguments" >> $LOG; fi

  if [ "$skipdb" == "-1" ] ;
  then
    #create folder if not exist
    [ ! -d $CF ] && mkdir -p $CF || :

    #clear old files
    clearOldFiles $CF
    if [ $? != 0 ]; then echo "[ERROR] clear $CF fail" >> $LOG; fi

    echo "backup $db" >> $LOG
    #create final file
    FILE="$CF/$db-$NOW.gz"
    mysqldump -h $SQLHOST -p$SQLPASS $db | gzip -9 > $FILE
    #sudo mysqldump $db 2>> $LOG | gzip -9 > $FILE 
    #check if success
    if [ $? == 0 ]; then echo "[OK] $db save" >> $LOG;
    else  echo "[ERROR] $db not save" >> $LOG; fi
   
  fi
done

#Backup shared folders#
echo "== shared==" >> $LOG
LWEB=$( ls -d $WF/*/ 2>> $LOG ) #List all the rails app directory 
if [ $? == 0 ]; #test if the rails app directory exist
then
  
  for project in $LWEB #for each project on the rails app directory
  do
    skipweb=-1
    #check if the project is on the blacklist
    blackList BLWEB $project skipweb
    if [ $? != 0 ]; then echo "[ERROR] check blackList $project, wrong number of arguments" >> $LOG; fi


    #back up this folder
    if [ "$skipweb" == -1 ];
    then
      SHARED="${project}$SF"
      #start the backup if the shared folder exist on the rails app
      if [ -d $SHARED ];
      then
	echo "-- start backup $(basename $SHARED) --" >> $LOG
	#create list of arguments for gzip
	GZARG=""

	#create folder if not exist
	CURRENTSHAREDBF="$SHAREDBF/$(basename $project)"
	[ ! -d  $CURRENTSHAREDBF ] && mkdir -p $CURRENTSHAREDBF || :
	#path of the backup final fine
	FILE="$CURRENTSHAREDBF/$(basename ${project%?})-$NOW.tar.gz" 2>> $LOG

	#clear old files
	clearOldFiles $CURRENTSHAREDBF
	if [ $? != 0 ]; then echo "[ERROR] clear $CURRENTSHAREDBF fail wrong number of arguments" >> $LOG; fi




	#create list of folder to add on the final file
	echo "+ add $SHARED on backup file " >> $LOG
	for folder in $WLSHARE
	do
	  #check if the folder on the white list exist
	  if [ $(find $SHARED -type d -name $folder | wc -l) -gt 0 ]; 
	  then
	    #add this folder on the arguments for create the tar files
	    GZARG="$GZARG $folder"
	    echo "|- add $folder" >> $LOG
	  else
	    echo "[WARN] $folder doesn't exist" >> $LOG
	  fi
	done
	
	#backup the share folder
	echo "compress file $FILE" >> $LOG
	tar cf - -C $SHARED $GZARG 2>> $LOG | gzip -9 > $FILE
	#check if success
	if [ $? == 0 ]; then echo "[OK] $FILE save" >> $LOG;
	else  echo "[ERROR] $FILE not save" >> $LOG; fi
      else
	echo "[WARN] $SHARED doesn't exist" >> $LOG
      fi
    fi
  done

else
  echo "[ERROR] can't list $WF" >> $LOG
fi



#clean temporary files
rm $LIMIT

