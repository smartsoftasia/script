#!/bin/bash

SIZE="xxhdpi"
MDPI=50
HDPI=75
XHDPI=100
XXHDPI=200

function getOptions() {

while getopts s: option
  do
	  case "${option}"
	  in
	          s) SIZE=${OPTARG}
		  echo "${OPTARG}"
		  ;;

		  h)
		  echo "use: ./resize -s <size> <folder> \n size can be mdpi, hdpi, xhdpi,
		  xxhdpi"
		  ;;
          esac
  done

}

function setSize {

  case "$SIZE" in
    "mdpi")
      MDPI=100
      HDPI=150
      XHDPI=200
      XXHDPI=400
      ;;
    "hdpi")
      MDPI=75
      HDPI=100
      XHDPI=125
      XXHDPI=250
      ;;
    "xhdpi")
      MDPI=50
      HDPI=75
      XHDPI=100
      XXHDPI=200
      ;;
    "xxhdpi") 
    MDPI=25
    HDPI=30
    XHDPI=50
    XXHDPI=100
    ;;
  esac

}


if [ $# -ge 1 -o $# -le 3 ] ;
then
  cd $3
  FILES="$(ls *.png)"

while getopts s: option
  do
	  case "${option}"
	  in
	          s) SIZE=${OPTARG}
		  echo "${OPTARG}"
		  ;;
          esac
  done

  getOptions "$@"
  setSize

  mkdir -p drawable-mdpi
  mkdir -p drawable-hdpi
  mkdir -p drawable-xhdpi
  mkdir -p drawable-xxhdpi

  for image in $FILES
  do
    echo "Resize $image"
    convert $image -resize ${MDPI}% -quality 100 drawable-mdpi/$image;
    convert $image -resize ${HDPI}% -quality 100 drawable-hdpi/$image;
    convert $image -resize ${XHDPI}% -quality 100 drawable-xhdpi/$image;
    convert $image -resize ${XXHDPI}% -quality 100 drawable-xxhdpi/$image;
  done

else
  echo "Wrong number of arguments, should be ./resize path"
fi
