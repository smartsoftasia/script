#!/bin/bash

#local repository
LOCALPATH="/home/slooblack/backup"
#Log file
LOG="synchro.log"

function synchronize {
  if [ $# == 4 ];
  then
    IP=$1; USER=$2 ; REMOTEPATH=$3 ; FOLDERPATH="$LOCALPATH/$4/" ;
    #create local path folder if not exist
    [ ! -d $FOLDERPATH ] && mkdir -p $FOLDERPATH || :

    echo "$USER$i@$IP$i:$REMOTEPATH$i $LOCALPATH"
    sudo rsync -e ssh -avz --delete-after $USER@$IP:$REMOTEPATH $FOLDERPATH
    if [ $? == 0 ];
    then
      echo "[OK] sync $IP" >> $LOG
    else
      echo "[ERROR] sync $IP fail, error number $?" >> $LOG
    fi

  else
    echo "[ERROR] function syncro: wrong number of arguments" >> $LOG
    return
  fi
}


#create local path folder if not exist
[ ! -d $LOCALPATH ] && mkdir -p $LOCALPATH || :

#HOW TO USE SYNCHRONIZE:
#synchronize ip user path folder_name
#/!\ Do not forget the "/" at the end of the path
#with / copy the content of the folder
#without / copy the folder 
#for add new server to synchronize add a new line
synchronize 178.79.138.76 root /var/www/backup/ linode2



