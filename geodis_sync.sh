#!/bin/bash

GEODIS=/home/slooblack/Developement/rails/geodis-backend

LOG=$GEODIS/log/rake_synchro.log

#Go on geodis backend directory
cd $GEODIS

echo "===============================================================" >> $LOG
echo "====== Synchromize database $(date) ======" >> $LOG
echo "===============================================================" >> $LOG
echo -e "\nimport database\n"  >> $LOG

#import table
#bundle exec rake synchro:articles >> $LOG
#bundle exec rake synchro:articles_pricing >> $LOG
#bundle exec rake synchro:articles_calculate_price >> $LOG
#bundle exec rake synchro:designs >> $LOG
#bundle exec rake synchro:metals >> $LOG
bundle exec rake synchro:clients >> $LOG
#bundle exec rake synchro:sub_clients >> $LOG
#bundle exec rake synchro:stone_types >> $LOG
#bundle exec rake synchro:stone_colors >> $LOG
bundle exec rake synchro:orders >> $LOG
bundle exec rake synchro:orders_items >> $LOG

#export table
#bundle exec rake synchro:collections >> $LOG
bundle exec rake synchro:export_orders >> $LOG
bundle exec rake synchro:export_client >> $LOG
bundle exec rake synchro:export_order_item >> $LOG



