Script
================

## Backup

* backup database and shared folder
    * backup.sh
    * pg_backup.sh
    * pg_backup.config
    * backup.log
    
* Copy backup folders into repo server
    * syncBackup.sh
    * synchro.log
    
## Geodis

* Import and export database tables
    * geodis_sync.sh
